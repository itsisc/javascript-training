//1.Variabile

let x = 2;
x = 4;
console.log("Let :", x);

const y = 2;
// y = 3;
console.log("Const :", y);

console.log(x + y);

//2.Array-uri

const z = [2, 5, 6];
z.push(10);
console.log("Const array:", z);

const arr = ["Cristi", "Astrid", "Robert"];

arr.push("Madalin");
console.log("Arr:", arr);

arr.pop();
console.log("Arr:", arr);

arr.shift();
console.log("Arr:", arr);

arr.unshift("Tibi");
console.log("Arr:", arr);

const objArr = [
  { label: "Cartof", value: "cartof", quantity: 5, price: 25 },
  { label: "Rosie", value: "rosie", quantity: 3, price: 5 },
  { label: "Castravete", value: "castravete", quantity: 10, price: 7 },
];

//Adaugam o proprietate noua in array
objArr.forEach((item) => {
  item.totalPrice = item.quantity * item.price;
});
console.log("For each:", objArr);

//Facem un nou array cu preturile totale
const newArray = objArr.map((item) => {
  item.totalPrice = item.quantity * item.price;
  return item.totalPrice;
});
console.log("New array:", newArray);
console.log("ObjArr:", objArr);

//Folosim metoda reduce pentr a calcula pretul total al produselor
const totalPriceArr = newArray.reduce((prev, next) => prev + next);
console.log("Total price arr:", totalPriceArr);

//Cu ajutorul destructurarii putem uni mai multe tipuri de array-uri in unul singur
const destructuredArray = [...objArr, ...newArray, "Tibi"];
console.log("Destructured Array:", destructuredArray);

//Cu ajutorul destructurarii putem crea un nou array cu o noua proprietate
const newArray2 = objArr.map((item, index) => {
  const newItem = { ...item, id: index };
  return newItem;
});
console.log("New Array2:", newArray2);

//3.Strings Tibi
let myName = "Tibi";

let string = "croco";
console.log("String:", string);

//Folosim interpolation pentru a putea face stringul dinamic mult mai usor
string = `croco ${myName}`;
console.log("String with interpolation:", string);

//Aici cred ca va prindeti de la ce vine toLowerCase :))
const string2 = "CROCO";
console.log("String toLowerCase:", string2.toLowerCase());

//Functia de split desparte string-ul intr-un array dupa elementul care este dat ca parametru
const string3 = "Cezin si Tibi sunt prosti";
const string3Splitted = string3.split(" ");
console.log("String splitted:", string3Splitted);

//Functia join construieste un string din elementele unui array, separatorul fiind parametrul dat prin metoda
console.log("String reunited:", string3Splitted.join(" "));

//4. DOM Manipulations

//document.getElementById selecteaza elementul cu id-ul dat ca parametru
const myElement = document.getElementById("myElement");

//Document.querySelector selecteaza primul element cu tipul dat ca parametru ('.class' selecteaza elementul cu clasa 'class' iar '#id' selecteaza elementul cu id-ul 'id')
let cristi = document.querySelector(".cristi");

//Document.querySelectorAll selecteaza toate elementele cu tipul dat ca parametru si face un array din ele.
let cristi2 = document.querySelectorAll(".cristi");

console.log("My Element:", myElement);
console.log("Cristi:", cristi);
console.log("Cristi2:", cristi2);

//Aici schimbam ce se afla in interiorul tagului de html, respectiv niste stil (font size/weight)
myElement.innerHTML = "Salut, Cristi e urât";
myElement.style.fontWeight = 700;
myElement.style.fontSize = "25px";

//Schimbam culoarea de background a unui element
cristi.style.backgroundColor = "red";

//In Cristi2 avem un array cu toate elementele cu clasa 'cristi' si le vom parcurge pe toate
cristi2.forEach((item) => {
  //Pentru fiecare element din array vom adauga, sterge si toggălui (cred ca asa se zice) clase
  item.classList.add("tibi");
  item.classList.add("cezin");
  item.classList.remove("tibi");
  item.classList.toggle("madalin");
  item.classList.toggle("madalin");
});

//5. Event listeners
let i = 1;
let j = 0;
//Selectam butonul nostru si ii adaugam un eventListener pe click
document.getElementById("btn").addEventListener("click", (e) => {
  i += 0.5;
  j++;
  //Introducem, cu ajutorul interpolarii, in divul corespunzator, numarul de click-uri pe buton
  document.querySelector(
    ".click-counter"
  ).innerHTML = `Your button has been clicked ${j} times`;
  //Acest e.target este parametrul dat in cadrul addEventListenerului, si este echivalentul a 'document.getElementById('btn')', adica
  e.target.style.transform = `scale(${i}) translate(${i * 10}px,${j * 10}px)`;
});

//Selectam input-ul si ii adaugam un eventListener de keyup (acest event este triggered cand userul da drumul unei taste)
document.getElementById("input-text").addEventListener("keyup", (e) => {
  myElement.innerHTML = `Salut, Cristi e ${e.target.value}`;
});

//6. Validations
document.getElementById("btn-submit").addEventListener("click", () => {
  //Ne creăm obiectul nostru de user cu cele 3 proprietati pe care le avem in form (nume,prenume,email)
  let user = {
    nume: document.getElementById("first-name").value,
    prenume: document.getElementById("last-name").value,
    email: document.getElementById("email").value,
  };
  if (!user.nume) {
    toastr.error("Nu ai completat numele!");
  } else if (!user.prenume) {
    toastr.error("Nu ai completat prenumele!");
  } else if (
    !user.email.match(/^[a-z0-9](\.?[a-z0-9]){5,}@g(oogle)?mail\.com$/)
  ) {
    toastr.error("Emailul nu e gmail:D");
  } else {
    toastr.success("Use adaugat. Multumesc, sa-ti trag la muie");
    //Daca un user este adaugat cu succes, resetam toate inputurile pentru ca un nou user sa poata fi introdus
    document.getElementById("first-name").value = "";
    document.getElementById("last-name").value = "";
    document.getElementById("email").value = "";
  }
});

//Cam asta a fost, sper ca ati inteles, daca nu, stiti unde ne gasiti. Va pupam (Tibi & Cezin)
